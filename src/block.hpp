/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef BLOCK_H
#define BLOCK_H 1

#include <stdio.h>

#include "xyz.hpp"
#include "rgb.hpp"

class Block {

private:

	class bXYZ* xyz;
	class RGB* rgb;

public:

	Block();
	~Block();

public:

	class bXYZ* getXYZ() const;
	
	void setXYZ(const class bXYZ*);
	  
	void setXYZ(int x, int y, int z);
	
	void translate(int dx, int dy, int dz);
	
	class RGB* getRGB() const;
	  
	void setRGB(const class RGB*);
	  
	void setRGB(float r, float g, float b);
	
  
};

#endif
