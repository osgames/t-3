/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "starfield.hpp"

/*****************************************************************************
 * Constructors / Deconstructor
 ****************************************************************************/

Starfield::Starfield(int count, float scrollSpeed, int pattern) {

  this->starCount = count;
  this->scrollSpeed = scrollSpeed;
  this->pattern = pattern;

  this->stars = (struct Star*)malloc(this->starCount * sizeof(struct Star));

  for (int i = 0; i < this->starCount; i++)
    this->initStar(i);

}

Starfield::~Starfield() {

  free(this->stars);

}

/*****************************************************************************
 * Public Instance Methods
 ****************************************************************************/

struct Star* Starfield::getStars() const {

  return this->stars;

}

int Starfield::getStarCount() const {

  return this->starCount;

}

void Starfield::setScrollSpeed(float scrollSpeed) {

  this->scrollSpeed = scrollSpeed;

}

void Starfield::setPattern(int pattern) {

  this->pattern = pattern;
  
  for (int i = 0; i < this->starCount; i++)
    this->initStar(i);
  

}

void Starfield::update() {

  for (int i = 0; i < this->starCount; i++) {

    this->stars[i].x += this->scrollSpeed*this->stars[i].p*this->stars[i].dx;
    this->stars[i].y += this->scrollSpeed*this->stars[i].p*this->stars[i].dy;

    switch (this->pattern) {

    case STARFIELD_SCROLL_DOWN:

      if (this->stars[i].y < -1)
	this->resetStar(i);

      break;

    case STARFIELD_CONVERGENCE:

      this->stars[i].p -= this->scrollSpeed / 2.5;

      if (this->stars[i].y < -1 || this->stars[i].y > 1 ||
	  this->stars[i].p < 0)

	this->resetStar(i);

      break;

    case STARFIELD_DIVERGENCE:

      this->stars[i].p += rand1() / 100;

      if (this->stars[i].y < -1 || this->stars[i].y > 1 ||
	  this->stars[i].p > 1)

	this->resetStar(i);

      break;

    case STARFIELD_JUMP_DRIVE:

      if (rand1() < 0.04)
	this->stars[i].x += 0.1;

      if (this->stars[i].x > 1)
	this->resetStar(i);

      break;

    case STARFIELD_FIRE:

      this->stars[i].p += this->scrollSpeed / 2.5;
      
      if (rand1() < 0.02)
	this->stars[i].p = -1;

      if (this->stars[i].y > 1 || this->stars[i].p < 0)
	this->resetStar(i);

      break;

    case STARFIELD_WAVES:

      this->stars[i].x -= this->scrollSpeed*this->stars[i].p*
	this->stars[i].dx;

      this->stars[i].p += this->stars[i].dx;

      if (this->stars[i].p > 1)
	this->stars[i].dx = -0.01;

      else if (this->stars[i].p < 0)
	this->stars[i].dx = 0.01;
      
      if (this->stars[i].y < -1)

	this->resetStar(i);

      break;

    case STARFIELD_SPARKLE:

      this->stars[i].x -= this->scrollSpeed*this->stars[i].p*
	this->stars[i].dx;

      this->stars[i].p += this->stars[i].dx;

      if (this->stars[i].p > 1)
	this->stars[i].dx = -0.01;

      else if(this->stars[i].p < 0)
	this->stars[i].dx = 0.01;

      break;

    case STARFIELD_HYPERSPACE:

      if (this->stars[i].x < -1 || this->stars[i].x > 1 ||
	  this->stars[i].y < -1 || this->stars[i].y > 1)

	this->resetStar(i);

      break;

    case STARFIELD_JUMPSPACE:

      this->stars[i].p -= 0.1;

      if (this->stars[i].x < -1)
	this->stars[i].dx = 1;

      else if (this->stars[i].x > 1)
	this->stars[i].dx = -1;

      if (this->stars[i].y < -1)
	this->stars[i].dy = 1;

      else if (this->stars[i].y > 1)
	this->stars[i].dy = -1;

      if (this->stars[i].p < 0)
	this->resetStar(i);

      break;

    case STARFIELD_SCROLL_RANDOM:

      this->stars[i].x += this->scrollSpeed*this->stars[i].p*
	this->stars[0].dx;

      this->stars[i].y += this->scrollSpeed*this->stars[i].p*
	this->stars[0].dy;

      if (rand1() < 0.0001) {

	float d = rand1();
	if (d < 0.25) {
	  stars[0].dx = 1;
	  stars[0].dy = 0;
	} else if (d < 0.5) {
	  stars[0].dx = -1;
	  stars[0].dy = 0;
	} else if (d < 0.75) {
	  stars[0].dx = 0;
	  stars[0].dy = 1;
	} else {
	  stars[0].dx = 0;
	  stars[0].dy = -1;
	}

      }

      if (this->stars[i].x < -1 || this->stars[i].x > 1 ||
	  this->stars[i].y < -1 || this->stars[i].y > 1)
	this->resetStar(i);

      break;

    }

  }

}

/*****************************************************************************
 * Private Instance Methods
 ****************************************************************************/

void Starfield::initStar(int i) {

  this->resetStar(i);
  this->stars[i].p = rand1();
  this->stars[i].x = rand2();
  this->stars[i].y = rand2();
   
}

void Starfield::resetStar(int i) {

  this->stars[i].p = rand1();
  
  switch (this->pattern) {

  case STARFIELD_SCROLL_DOWN:

    this->stars[i].x = rand2();
    this->stars[i].dx = 0;

    this->stars[i].y = 1;
    this->stars[i].dy = -1;

    break;
    
  case STARFIELD_CONVERGENCE:
    
    this->stars[i].x = rand2();
    this->stars[i].dx = 0;

    if (rand1() < 0.5) {

      this->stars[i].y = 1;
      this->stars[i].dy = -1;

    } else {

      this->stars[i].y = -1;
      this->stars[i].dy = 1;

    }
    
    break;

  case STARFIELD_DIVERGENCE:

    this->stars[i].x = rand2();
    this->stars[i].dx = 0;

    this->stars[i].y = 0;

    if (rand1() < 0.5)
      this->stars[i].dy = -1;

    else
      this->stars[i].dy = 1;

    this->stars[i].p = 0;

    break;

  case STARFIELD_JUMP_DRIVE:

    this->stars[i].x = -1;
    this->stars[i].dx = 1;

    this->stars[i].y = rand2();
    this->stars[i].dy = 0;
        
    break;

  case STARFIELD_FIRE:

    this->stars[i].x = rand2();
    this->stars[i].dx = 0;

    this->stars[i].y = -1;
    this->stars[i].dy = 1;
   
    break;

  case STARFIELD_WAVES:

    this->stars[i].x = rand2();

    if (rand1() < 0.8)
      this->stars[i].dx = -0.01;
    else
      this->stars[i].dx = 0.0;

    this->stars[i].y = 1;
    this->stars[i].dy = -1;

    break;

  case STARFIELD_SPARKLE:

    this->stars[i].x = rand2();
    this->stars[i].dx = -0.01;

    this->stars[i].y = rand2();
    this->stars[i].dy = 0;

    break;

  case STARFIELD_HYPERSPACE:

    this->stars[i].x = rand2();

    if (this->stars[i].x < 0)
      this->stars[i].dx = -1;
    else
      this->stars[i].dx = 1;

    this->stars[i].y = rand2();

    if (this->stars[i].y < 0)
      this->stars[i].dy = -1;
    else
      this->stars[i].dy = 1;

    break;

  case STARFIELD_JUMPSPACE:

    this->stars[i].x = rand2();

    if (this->stars[i].x < 0)
      this->stars[i].dx = -1;
    else
      this->stars[i].dx = 1;

    this->stars[i].y = rand2();

    if (this->stars[i].y < 0)
      this->stars[i].dy = -1;
    else
      this->stars[i].dy = 1;

    break;

  case STARFIELD_SCROLL_RANDOM:

    if (rand1() < 0.5) {

      if (this->stars[0].dx == -1) {

	this->stars[i].x = 1;
	this->stars[i].y = rand2();

      } else if (this->stars[0].dx == 1) {

	this->stars[i].x = -1;
	this->stars[i].y = rand2();

      } else if (this->stars[0].dy == -1) {
	
	this->stars[i].x = rand2();
	this->stars[i].y = 1;

      } else if (this->stars[0].dy == 1) {
	
	this->stars[i].x = rand2();
	this->stars[i].y = -1;

      }

    } else {

      this->stars[i].x = rand2();
      this->stars[i].y = rand2();

    }

    if (i != 0) {
      this->stars[i].dx = 0;
      this->stars[i].dy = 0;
    }

    break;



  }

}
