/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef XYZ_H
#define XYZ_H 1

#include <stdio.h>

class wXYZ {

private:

  float x;
  float y;
  float z;

public:

  wXYZ(float x = 0.0, float y = 0.0, float z = 0.0);

public:

  void setXYZ(const class wXYZ*);
  void setXYZ(float x, float y, float z);

  void setX(float x);
  float getX() const;

  void setY(float y);
  float getY() const;

  void setZ(float z);
  float getZ() const;

  class bXYZ asBlockXYZ() const;

  void print() const;

};

class bXYZ {

private:

  int x;
  int y;
  int z;

public:

  bXYZ(int x = 0, int y = 0, int z = 0);

public:

  void setXYZ(const class bXYZ*);
  void setXYZ(int x, int y, int z);

  void setX(int x);
  int getX() const;

  void setY(int y);
  int getY() const;

  void setZ(int z);
  int getZ() const;

  class wXYZ asWorldXYZ() const;
  
  void print() const;

};

#endif
