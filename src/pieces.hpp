/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef PIECES_H
#define PIECES_H 1

#include <stdlib.h>
#include <iostream>

#include "block.hpp"
#include "xyz.hpp"
#include "rgb.hpp"

class Piece {

protected:

  class Block* blocks[4];
  class bXYZ* minXYZ;
  class bXYZ* maxXYZ;

private:

  float animXRotation;
  float animYRotation;
  float animZRotation;
  float animYTranslation;

public:

	Piece();
	~Piece();

public:

  class Block* getBlock(int i) const;

  class bXYZ* getMinXYZ() const;
  
  class bXYZ* getMaxXYZ() const;

  void setRGB(const class RGB*);
  
  void setRGB(float r, float g, float b);

  void translate(int dx, int dy, int dz);
  
  void rotate(int rx, int ry, int rz);
  
  float getAnimXRotation() const;
  
  void setAnimXRotation(float);

  float getAnimYRotation() const;
  
  void setAnimYRotation(float);

  float getAnimZRotation() const;
  
  void setAnimZRotation(float);

  float getAnimYTranslation() const;
  
  void setAnimYTranslation(float);

private:

  void calculateMinXYZ();
  
  void calculateMaxXYZ();

};

class IPiece : public Piece {

public:
  
  IPiece();

};

class OPiece : public Piece {

public:

  OPiece();

};

class ZPiece : public Piece {

public:

  ZPiece();

};

class TPiece : public Piece {

public:
  
  TPiece();

};

class JPiece : public Piece {

public:

  JPiece();

};

class Squidget : public Piece {

public:
  
  Squidget();

};

class Widget : public Piece {

public:

  Widget();

};

#endif
