/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "block.hpp"

/*****************************************************************************
 * Constructors / Deconstructor
 ****************************************************************************/

Block::Block() {

	this->xyz = new bXYZ();
	this->rgb = new RGB();

}

Block::~Block() {

	delete this->xyz;
	delete this->rgb;

}

/*****************************************************************************
 * Public Instance Methods
 ****************************************************************************/

class bXYZ* Block::getXYZ() const {

	return this->xyz;

}

void Block::setXYZ(const class bXYZ* xyz) {

	this->xyz->setXYZ(xyz);

}

void Block::setXYZ(int x, int y, int z) {

	this->xyz->setXYZ(x, y, z);

}

void Block::translate(int dx, int dy, int dz) {

	this->xyz->setXYZ(
		this->xyz->getX() + dx,
		this->xyz->getY() + dy,
		this->xyz->getZ() + dz);
		
}

class RGB* Block::getRGB() const {

	return this->rgb;

}

void Block::setRGB(const class RGB* rgb) {

	this->rgb->setRGB(rgb);

}

void Block::setRGB(float r, float g, float b) {

	this->rgb->setRGB(r, g, b);

}
