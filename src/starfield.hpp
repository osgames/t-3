/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef STARFIELD_H
#define STARFIELD_H 1

#include <stdlib.h>
#include <math.h>

typedef struct Star {

  float x; float y;
  float dx; float dy;
  float p;

} Star;

class Starfield {

public:

  static const int STARFIELD_SCROLL_DOWN = 0;
  static const int STARFIELD_SCROLL_RANDOM = 1;
  static const int STARFIELD_HYPERSPACE = 2;
  static const int STARFIELD_CONVERGENCE = 3;
  static const int STARFIELD_WAVES = 4;
  static const int STARFIELD_FIRE = 5;
  static const int STARFIELD_JUMP_DRIVE = 6;
  static const int STARFIELD_JUMPSPACE = 7;
  static const int STARFIELD_DIVERGENCE = 8;
  static const int STARFIELD_SPARKLE = 9;


private:

  struct Star* stars;
  int starCount;
  int pattern;
  float scrollSpeed;

public:

  Starfield(int count = 200, float scrollSpeed = 0.005, int pattern = 0);
  ~Starfield();

public:

  struct Star* getStars() const;
  int getStarCount() const;
  void setScrollSpeed(float scrollSpeed);
  void setPattern(int pattern);
  void update();

private:

  void initStar(int i);
  void resetStar(int i);

private:

  static float rand1() { return rand() / (RAND_MAX + 1.0); }
  static float rand2() { return ((rand() / (RAND_MAX + 1.0)) * 2) - 1; }

};

#endif
