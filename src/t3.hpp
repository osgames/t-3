/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef T3_H
#define T3_H 1

#include <list>
#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>
#include <tiffio.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>

#include "starfield.hpp"
#include "pieces.hpp"
#include "block.hpp"
#include "xyz.hpp"
#include "rgb.hpp"

using std::list;

class T3 {

public:

	static void start(int, char**);
	
private:

	static void reset();
	static void cleanup();
	static void idle();
	
	// Display Functions
	static void drawScreen();
	static void drawStarfield();
	static void drawPlayingColumn();
	static void drawPlayingColumnFloor();
	static void drawPlayingColumnPosts();
	static void drawScoreboards();
	static void drawLevelIndicators();
	static void drawLandedBlocks();
	static void drawGhostBlocks();
	static void drawPiece(class Piece*, bool, bool);
	static void drawDropShadow();
	static void drawBlock(const class Block*, bool, bool);
	static void drawText(float, float, float, int, bool, char* str);
	static void handleWindowResizeEvent(int width, int height);
	
	// Occlusion & Culling Functions
	static void updateVisibleBlocks();
	static void updateVisibleBlockSurfaces();
	
	// Player Control Functions
	static void setPaused(bool paused);
	static void handleKeyboardEvent(int key, int x, int y);
	static void handleKeyboardEvent(unsigned char key, int x, int y);
	static void handleMouseMotionEvent(int x, int y);
	static void handleMouseEvent(int button, int state, int x, int y);
	
	// Piece Related Functions
	static bool spawnPiece();
	static class Piece* generateNewPiece();
	static bool requestPieceTranslation(int dx, int dy, int dz);
	static bool requestPieceRotation(int rx, int ry, int rz);
	static void handlePieceLanding();
	
	// Collision Detection Functions
	static bool collisionWithBoundaries();
	static class Block* collisionWithLandedBlocks();
	
	// Utility Functions
	static void loadTextureFromTIFF(GLuint* textureName, char* filename);
	
	/* The RGB values used to color pieces and blocks. */
	static const float T3::colorScheme[7][3];

	/* These rotation matrices are used to shift the keyboard controls in
	 * response to viewing angle changes. */
	static const int T3::wasdRotationMatrix[8][8];
	static const int T3::qerzRotationMatrix[8][20];

	/* Player status variables. */
	static long playerScore;
	static int playerLevel;
	static long playerTimer;
	static int playerTimeRemaining;
	static int playerTimeAccumulated;
	static int playerSurfacesCompleted;
	static long playerScoreTicker;
	static int playerTimeTicker;
	
	/* Piece control variables. */
	static class Piece* nextPiece;
	static float nextPieceRotation;
	static class Piece* currentPiece;
	static long pieceTimer;
	static int pieceFallIterations;
	
	/* Landed/surface/ghost block variables. */
	static int surfaceColorOffset;
	static bool isSurfaceAlive[20];
	static float surfaceYTranslation[20];
	static list<class Block*> ghostBlocks;
	static float ghostBlockAlpha;
	static class Block* landedBlocks[20][10][10];

	/* Floor/post block variables. */
	static class Block* postBlocks[21][4];
	static class Block* floorBlocks[10][10];

	/* Game control variables. */	
	static bool isPaused;
	static bool isGameOver;
	static bool isTimerEnabled;
	static long firstClockTick;
	static long frameCount;
	static int gameSize;

	/* Window variables. */
	static int window;
	static int windowWidth;
	static int windowHeight;
	static int oldWindowWidth;
	static int oldWindowHeight;
	static bool isFullScreen;

	/* Display variables. */
	static GLuint blockTexture;
	static GLuint charTextures[38]; // Null, A..Z, 0..9, :
	static float starMagentaFlash;
	static float starGreenFlash;	
	static class Starfield* starfield;
	static float playingFieldTilt;
	static float playingFieldRotation;	
	static bool showTimeOnScoreboard;
	static bool showFPSOnScoreboard;
	static bool visibleBlockSurfaces[6]; // Top, Btm, Lft, Rght, Frnt, Bck
	static list<class Block*> visibleBlocks;
	
	/* Mouse control variables. */
	static int mouseLastX;
	static int mouseLastY;
	static int mouseMarkedX;
	static int mouseMarkedY;
	static int mouseActiveButton;
	
	static class Piece* backgroundPieces[50];
		
};

int main(int argc, char** argv);

#endif 
