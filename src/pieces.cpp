/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "pieces.hpp"

/*****************************************************************************
 * Constructors / Deconstructor
 ****************************************************************************/

Piece::Piece() {

for (int i = 0; i < 4; i++)
		this->blocks[i] = new Block();

	this->minXYZ = new bXYZ();
	this->maxXYZ = new bXYZ();
	
	this->animXRotation = 0.0;
	this->animYRotation = 0.0;
	this->animZRotation = 0.0;
	this->animYTranslation = 0.0;

}

Piece::~Piece() {

	for (int i = 0; i < 4; i++)
		delete this->blocks[i];

	delete this->minXYZ;
	delete this->maxXYZ;

}

/*****************************************************************************
 * Public Instance Methods
 ****************************************************************************/

class Block* Piece::getBlock(int i) const {

	if (i < 0 || i > 4)
		return NULL;
	
	else
		return this->blocks[i];

}

class bXYZ* Piece::getMinXYZ() const {

	return this->minXYZ;

}

class bXYZ* Piece::getMaxXYZ() const {

	return this->maxXYZ;

}

void Piece::setRGB(const class RGB* rgb) {

	for (int i = 0; i < 4; i++)
		this->blocks[i]->setRGB(rgb);

}

void Piece::setRGB(float r, float g, float b) {

	for (int i = 0; i < 4; i++)
		this->blocks[i]->setRGB(r, g, b);

}

void Piece::translate(int dx, int dy, int dz) {

for (int i = 0; i < 4; i++)

	this->blocks[i]->setXYZ
		(this->blocks[i]->getXYZ()->getX() + dx,
		this->blocks[i]->getXYZ()->getY() + dy,
		this->blocks[i]->getXYZ()->getZ() + dz);

	this->minXYZ->setXYZ
	(this->minXYZ->getX() + dx,
	this->minXYZ->getY() + dy,
	this->minXYZ->getZ() + dz);
	
	this->maxXYZ->setXYZ
	(this->maxXYZ->getX() + dx,
	this->maxXYZ->getY() + dy,
	this->maxXYZ->getZ() + dz);
    
}

void Piece::rotate(int rx, int ry, int rz) {

  class bXYZ* xyz = this->blocks[0]->getXYZ();
  int dx = xyz->getX(); int dy = xyz->getY(); int dz = xyz->getZ();

  int oldMinY = this->minXYZ->getY();

  // Translate piece to the origin

  this->translate(-dx, -dy, -dz);

  // Reflect each block in piece

  for (int i = 0; i < 4; i++) {

    xyz = this->blocks[i]->getXYZ();
    int x = xyz->getX(); int y = xyz->getY(); int z = xyz->getZ();

    this->blocks[i]->setXYZ
      (rx == 0 ? x : ry != 0 ? ry * y : rz * z,
       ry == 0 ? y : rz != 0 ? rz * z : rx * x,
       rz == 0 ? z : rx != 0 ? rx * x : ry * y);

  }

  // Return piece to original location

  this->translate(dx, dy, dz);

  // Recalculate minimum & maximum XYZs
  
  this->calculateMinXYZ();
  this->calculateMaxXYZ();

  /* If the bottom edge of the piece has changed as a result of rotation,
     translate the piece to compensate. */
  
  int newMinY = this->minXYZ->getY();

  if (newMinY != oldMinY)
    this->translate(0, (oldMinY - newMinY), 0);

}

float Piece::getAnimXRotation() const {

  return Piece::animXRotation;

}

void Piece::setAnimXRotation(float rx) {

  Piece::animXRotation = rx;

}

float Piece::getAnimYRotation() const {

  return Piece::animYRotation;

}

void Piece::setAnimYRotation(float ry) {

  Piece::animYRotation = ry;

}

float Piece::getAnimZRotation() const {

  return Piece::animZRotation;

}

void Piece::setAnimZRotation(float rz) {

  Piece::animZRotation = rz;

}

float Piece::getAnimYTranslation() const {

  return Piece::animYTranslation;

}

void Piece::setAnimYTranslation(float dy) {

  Piece::animYTranslation = dy;

}

/*****************************************************************************
 * Private Instance Methods
 ****************************************************************************/

void Piece::calculateMinXYZ() {

  class bXYZ* xyz = this->blocks[0]->getXYZ();
  int min_x = xyz->getX(); int min_y = xyz->getY(); int min_z = xyz->getZ();

  for (int i = 1; i < 4; i++) {

    xyz = this->blocks[i]->getXYZ();

    if (xyz->getX() < min_x)
      min_x = xyz->getX();

    if (xyz->getY() < min_y)
      min_y = xyz->getY();

    if (xyz->getZ() < min_z)
      min_z = xyz->getZ();

  }

  this->minXYZ->setXYZ(min_x, min_y, min_z);

}

void Piece::calculateMaxXYZ() {

  class bXYZ* xyz = this->blocks[0]->getXYZ();
  int max_x = xyz->getX(); int max_y = xyz->getY(); int max_z = xyz->getZ();

  for (int i = 1; i < 4; i++) {

    xyz = this->blocks[i]->getXYZ();

    if (xyz->getX() > max_x)
      max_x = xyz->getX();

    if (xyz->getY() > max_y)
      max_y = xyz->getY();

    if (xyz->getZ() > max_z)
      max_z = xyz->getZ();

  }

  this->maxXYZ->setXYZ(max_x, max_y, max_z);

}

/*****************************************************************************
 * I-Piece
 *
 *  +--+
 * /--/|
 * |  |/
 * +--+|
 * |()|/
 * +--+|
 * |  |/
 * +--+|
 * |  |/
 * +--+
 *
 * []
 * []
 * []
 * []  [][][][]
 *
 ****************************************************************************/

IPiece::IPiece() : Piece() {

  this->blocks[0]->setXYZ(0, 0, 0);
  this->blocks[1]->setXYZ(0, -1, 0);
  this->blocks[2]->setXYZ(0, 1, 0);
  this->blocks[3]->setXYZ(0, 2, 0);

  this->minXYZ->setXYZ(0, -1, 0);
  this->maxXYZ->setXYZ(0, 2, 0);

}

/*****************************************************************************
 * O-Piece
 *
 *  +--+--+
 * /--/--/|
 * |()|  |/
 * +--+--+|
 * |  |  |/
 * +--+--+
 *
 * [][]
 * [][]
 *
 ****************************************************************************/

OPiece::OPiece() : Piece() {

  this->blocks[0]->setXYZ(0, 0, 0);
  this->blocks[1]->setXYZ(0, 1, 0);
  this->blocks[2]->setXYZ(1, 1, 0);
  this->blocks[3]->setXYZ(1, 0, 0);

  this->minXYZ->setXYZ(0, 0, 0);
  this->maxXYZ->setXYZ(1, 1, 0);

}

/*****************************************************************************
 * Z-Piece
 *
 *  +--+--+
 * /--/--/|
 * |  |()|/--+
 * +--+--+--/|
 *    |  |  |/
 *    +--+--+
 *
 *         []              []
 *   [][]  [][]  [][]    [][]
 * [][]      []    [][]  []
 *
 ****************************************************************************/

ZPiece::ZPiece() : Piece() {

  this->blocks[0]->setXYZ(0, 0, 0);
  this->blocks[1]->setXYZ(0, 1, 0);
  this->blocks[2]->setXYZ(1, 0, 0);
  this->blocks[3]->setXYZ(1, -1, 0);

  this->minXYZ->setXYZ(0, -1, 0);
  this->maxXYZ->setXYZ(1, 1, 0);

}

/*****************************************************************************
 * T-Piece
 *
 *  +--+--+--+
 * /--/--/--/|
 * |  |()|  |/
 * +--+--+--+
 *    |  ||
 *    +--+/
 *
 * []              []
 * [][]  [][][]  [][]    []
 * []      []      []  [][][]
 *
 ****************************************************************************/

TPiece::TPiece() : Piece() {

  this->blocks[0]->setXYZ(0, 0, 0);
  this->blocks[1]->setXYZ(0, -1, 0);
  this->blocks[2]->setXYZ(0, 1, 0);
  this->blocks[3]->setXYZ(1, 0, 0);

  this->minXYZ->setXYZ(0, -1, 0);
  this->maxXYZ->setXYZ(1, 1, 0);

}

/*****************************************************************************
 * J-Piece
 *
 *     +--+
 *    /--/|
 *    |  |/
 *    +--+|
 *  +-|()|/
 * /--+--+|
 * |  |  |/
 * +--+--+
 *
 *    []          [][]          []            [][]
 *    []  []      []    [][][]  []    [][][]    []      []
 *  [][]  [][][]  []        []  [][]  []        []  [][][]
 *
 ****************************************************************************/

JPiece::JPiece() : Piece() {
  
  this->blocks[0]->setXYZ(0, 0, 0);
  this->blocks[1]->setXYZ(0, 1, 0);
  this->blocks[2]->setXYZ(0, -1, 0);
  this->blocks[3]->setXYZ(-1, -1, 0);

  this->minXYZ->setXYZ(-1, -1, 0);
  this->maxXYZ->setXYZ(0, 1, 0);

}

/*****************************************************************************
 * Squidget
 *
 *      +--+
 *     /--/|
 *     |  |/
 *  +--+--/|
 * /--/--/|/
 * |  |  |/
 * +--+--+
 *
 *   []  []    [][]  [][]
 * [][]  [][]  []      []
 *
 ****************************************************************************/

Squidget::Squidget() : Piece() {
  
  this->blocks[0]->setXYZ(0, 0, 0);
  this->blocks[1]->setXYZ(-1, 0, 0);
  this->blocks[2]->setXYZ(0, 0, 1);
  this->blocks[3]->setXYZ(0, 1, 1);

  this->minXYZ->setXYZ(-1, 0, 0);
  this->maxXYZ->setXYZ(0, 1, 1);

}

/*****************************************************************************
 * Widget
 *
 *      +--+
 *  +--/--/|
 * /--/--/|/
 * |  |  |/
 * +--+--+|
 *    |  |/
 *    +--+
 *
 *   []  []    [][]  [][]
 * [][]  [][]  []      []
 *
 ****************************************************************************/

Widget::Widget() : Piece() {
  
  this->blocks[0]->setXYZ(0, 0, 0);
  this->blocks[1]->setXYZ(1, 0, 0);
  this->blocks[2]->setXYZ(0, 1, 0);
  this->blocks[3]->setXYZ(0, 0, 1);

  this->minXYZ->setXYZ(0, 0, 0);
  this->maxXYZ->setXYZ(1, 1, 1);

}
