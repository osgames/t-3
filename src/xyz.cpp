/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "xyz.hpp"

/*****************************************************************************
 * Constructors / Deconstructor
 ****************************************************************************/

wXYZ::wXYZ(float x, float y, float z) {

  this->x = x;
  this->y = y;
  this->z = z;

}

/*****************************************************************************
 * Public Instance Methods
 ****************************************************************************/

void wXYZ::setXYZ(const class wXYZ* xyz) {

  this->x = xyz->x;
  this->y = xyz->y;
  this->z = xyz->z;

}

void wXYZ::setXYZ(float x, float y, float z) {

  this->setX(x);
  this->setY(y);
  this->setZ(z);

}

void wXYZ::setX(float x) {

  this->x = x;

}

float wXYZ::getX() const {

  return this->x;

}

void wXYZ::setY(float y) {

  this->y = y;

}

float wXYZ::getY() const {

  return this->y;

}

void wXYZ::setZ(float z) {

  this->z = z;

}

float wXYZ::getZ() const {

  return this->z;

}

class bXYZ wXYZ::asBlockXYZ() const {

  int x = (int)((this->x - 0.05) * 10.0) + 5;
  int y = (int)((this->y - 0.05) * 10.0) + 10;
  int z = (int)((this->z + 0.05) * -10.0) + 5;

  return bXYZ(x, y, z);

}

void wXYZ::print() const {

  printf("(%f, %f, %f)", this->x, this->y, this->z);

}

bXYZ::bXYZ(int x, int y, int z) {

  this->x = x;
  this->y = y;
  this->z = z;

}

void bXYZ::setXYZ(const class bXYZ* xyz) {

  this->x = xyz->x;
  this->y = xyz->y;
  this->z = xyz->z;

}

void bXYZ::setXYZ(int x, int y, int z) {

  this->setX(x);
  this->setY(y);
  this->setZ(z);

}

void bXYZ::setX(int x) {

  this->x = x;

}

int bXYZ::getX() const {

  return this->x;

}

void bXYZ::setY(int y) {

  this->y = y;

}

int bXYZ::getY() const {

  return this->y;

}

void bXYZ::setZ(int z) {

  this->z = z;

}

int bXYZ::getZ() const {

  return this->z;

}

class wXYZ bXYZ::asWorldXYZ() const {

  float x = ((this->x - 5) / 10.0) + 0.05;
  float y = ((this->y - 10) / 10.0) + 0.05;
  float z = ((this->z - 5) / -10.0) - 0.05;

  return wXYZ(x, y, z);

}

void bXYZ::print() const {

  printf("(%i, %i, %i)", this->x, this->y, this->z);

}
