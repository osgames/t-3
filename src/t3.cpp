/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "t3.hpp"
/* ^_ please refer to this file for function/variable documentation. */

int main(int argc, char** argv) {

	/* Seed the random number generator. */
	srand((unsigned)time(0));

	/* Display copyright information. */
	printf
	("T^3 - A 3-dimensional Tetris(tm) game\n\n");
	
	printf
	("Copyright (C) 2004 Derek Hausauer\n");
	
	printf
	("http://t-3.sourceforge.net\n\n");
	
	printf
	("This program is free software; you can redistribute it and/or modify\n");
	
	printf
	("it under the terms of the GNU General Public License as published by\n");
	
	printf
	("the Free Software Foundation; either version 2 of the License, or\n");
	
	printf
	("(at your option) any later version.\n\n");
	
	printf
	("This program is distributed in the hope that it will be useful,\n");
	
	printf
	("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
	
	printf
	("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
	
	printf
	("GNU General Public License (LICENSE.TXT) for more details.\n\n");
	
	printf
	("You should have received a copy of the GNU General Public License\n");
	
	printf
	("along with this program; if not, write to the Free Software\n");
	
	printf
	("Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA\n");

	/* Start T^3. */
	T3::start(argc, argv);
	return 1;

}

void T3::start(int argc, char** argv) {
  
	/* GLUT Init */
	glutInitWindowSize(T3::windowWidth, T3::windowHeight);
	glutInitWindowPosition(100, 100);
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
	T3::window = glutCreateWindow("T^3");
	glutFullScreen();
	
	/* GLUT Init: Register Callbacks */
	glutIdleFunc(T3::idle); 
	glutDisplayFunc(T3::drawScreen);
	glutReshapeFunc(T3::handleWindowResizeEvent);
	glutKeyboardFunc(T3::handleKeyboardEvent);
	glutSpecialFunc(T3::handleKeyboardEvent);
	glutMouseFunc(T3::handleMouseEvent);
	glutMotionFunc(T3::handleMouseMotionEvent);
	glutPassiveMotionFunc(T3::handleMouseMotionEvent);
	
	/* GLUT Init: Pointer */
	glutSetCursor(GLUT_CURSOR_NONE);
	
	/* OpenGL Init */
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClearDepth(1.0);
	
	/* OpenGL Init: Shading & Filling */
	glEnable(GL_COLOR_MATERIAL);
	glShadeModel(GL_SMOOTH);
	glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	
	/* OpenGL Init: Antialiasing */
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_FASTEST);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_FASTEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	
	/* OpenGL Init: Texture Mapping */
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	T3::loadTextureFromTIFF(&T3::blockTexture, "data/blocks.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[0], "data/null.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[1], "data/A.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[4], "data/d.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[5], "data/E.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[6], "data/F.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[7], "data/G.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[12], "data/L.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[13], "data/M.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[15], "data/O.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[16], "data/P.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[18], "data/R.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[19], "data/S.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[21], "data/U.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[22], "data/V.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[27], "data/0.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[28], "data/1.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[29], "data/2.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[30], "data/3.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[31], "data/4.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[32], "data/5.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[33], "data/6.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[34], "data/7.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[35], "data/8.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[36], "data/9.tif");
	T3::loadTextureFromTIFF(&T3::charTextures[37], "data/colon.tif");
	
	/* OpenGL Init: Lighting */
	glEnable(GL_LIGHTING);
	
	GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	
	GLfloat mat_shininess[] = {50.0};
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	
	GLfloat light_position[] = {100.0, 0.0, 100.0, 0.0};
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	
	GLfloat light_color[] = {1.0, 1.0, 1.0, 0.0};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_color);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_color);
	
	glEnable(GL_LIGHT0);
	
	/* Set game mode depending on command line switches */

	for (int i = 1; i < argc; i++) {
		
		if (strcmp(argv[i], "-4") == 0) {
			T3::gameSize = 4;}
			
		if (strcmp(argv[i], "-6") == 0) {
			T3::gameSize = 6;}
			
		if (strcmp(argv[i], "-8") == 0) {
			T3::gameSize = 8;}
			
		if (strcmp(argv[i], "-10") == 0) {
			T3::gameSize = 10;}
			
		if (strcmp(argv[i], "-t") == 0) {
			T3::isTimerEnabled = true;
			T3::showTimeOnScoreboard = true;			
		}		
		
	}
	
	/* Initialize floor blocks. */
	for (int i = 0; i < T3::gameSize; i++) {
		
		for (int j = 0; j < T3::gameSize; j++) {
			
			T3::floorBlocks[i][j] = new class Block();
			T3::floorBlocks[i][j]->setXYZ(i, -1, j);
			T3::floorBlocks[i][j]->setRGB(0.5, 0.5, 0.5);
			
		}
		
	}
	
	/* Initialize post blocks. */
	for (int i = 0; i < 21; i++) {
	
		for (int j = 0; j < 4; j++) {
			
			T3::postBlocks[i][j] = new class Block();
			T3::postBlocks[i][j]->setRGB(0.5, 0.5, 0.5);
			
		}
	
		T3::postBlocks[i][0]->setXYZ(-1, i - 1, -1);
		T3::postBlocks[i][1]->setXYZ(-1, i - 1, T3::gameSize);
		T3::postBlocks[i][2]->setXYZ(T3::gameSize, i - 1, T3::gameSize);
		T3::postBlocks[i][3]->setXYZ(T3::gameSize, i - 1, -1);

	}

	for (int i = 0; i < 50; i++) {
		
		T3::backgroundPieces[i] = T3::generateNewPiece();
		
	}

	/* Call reset to initialize all other variables. */
	T3::reset();

	/* Enter the main loop. */
	glutMainLoop();

}

void T3::reset() {

	if (T3::starfield != NULL)
		delete T3::starfield;
	
	if (T3::visibleBlocks.size() != 0)
		T3::visibleBlocks.clear();
	
	for (int i = 0; i < 20; i++)
		for (int j = 0; j < T3::gameSize; j++)
			for (int k = 0; k < T3::gameSize; k++)
				if (T3::landedBlocks[i][j][k] != NULL) {
					delete T3::landedBlocks[i][j][k];
					T3::landedBlocks[i][j][k] = NULL;
				}
	
	if (T3::ghostBlocks.size() != 0) {
	
		for (list<class Block*>::const_iterator i=T3::ghostBlocks.begin();
			 i != T3::ghostBlocks.end(); ++i)
		delete *i;
	
		T3::ghostBlocks.clear();
	
	} 
	
	T3::playerLevel = 0;
	T3::playerScore = 0;
	T3::pieceFallIterations = 0;
	T3::playerSurfacesCompleted = 0;
	T3::playerScoreTicker = 0;
	
	T3::isPaused = false;
	T3::isGameOver = false;
	
	T3::playerTimeAccumulated = 0;
	T3::playerTimeRemaining = 2 * T3::gameSize * T3::gameSize;
	T3::playerTimeTicker = T3::playerTimeRemaining;
	
	T3::frameCount = 0;
	
	T3::starfield = new class Starfield(500, 0.0025, T3::playerLevel);
	
	T3::starMagentaFlash = 0.0;
	T3::starGreenFlash = 0.0;
	
	for (int i = 0; i < 20; i++) {
		T3::isSurfaceAlive[i] = true;
		T3::surfaceYTranslation[i] = 0.0;
		for (int j = 0; j < T3::gameSize; j++)
			for (int k = 0; k < T3::gameSize; k++)
				T3::landedBlocks[i][j][k] = NULL;
	}
		
	T3::ghostBlockAlpha = 0.0;
	
	T3::firstClockTick = clock();
	
	if (T3::nextPiece != NULL)
		delete (T3::nextPiece);
		
	T3::nextPiece = T3::generateNewPiece();
	
	T3::spawnPiece();
	  
}

void T3::cleanup() {

  T3::reset();

  delete T3::currentPiece;

  delete T3::starfield;

  for (int i = 0; i < T3::gameSize; i++)
    for (int j = 0; j < T3::gameSize; j++)
      delete T3::floorBlocks[i][j];

  for (int i = 0; i < 21; i++)
    for (int j = 0; j < 4; j++)
      delete T3::postBlocks[i][j];

}

void T3::idle() {

	T3::starfield->update();

	if (!T3::isPaused) {
  
		if ((clock() - T3::pieceTimer) / (float)CLOCKS_PER_SEC >=
			((10.0 - T3::playerLevel) / 20.0)) {
	
			if (T3::requestPieceTranslation(0, -1, 0)) {
		
				T3::pieceFallIterations++;
				T3::currentPiece->setAnimYTranslation(0.1);
				T3::pieceTimer = clock();
	
		     } else
	      
				T3::handlePieceLanding();
				
		}

    if ((clock() - T3::playerTimer) / (float)CLOCKS_PER_SEC >= 1.0) {

      T3::playerTimeAccumulated++;

      if (T3::isTimerEnabled) {

	T3::playerTimeRemaining--;

	if (T3::playerTimeRemaining == 0) {
	  T3::isPaused = true;
	  T3::isGameOver = true;
	}

      }

      T3::playerTimer = clock();

    }

  }
  
  float yTrans = T3::currentPiece->getAnimYTranslation();  
  if (yTrans > 0.0)
    T3::currentPiece->setAnimYTranslation(yTrans - 0.01);

  float xRot = T3::currentPiece->getAnimXRotation();
  if (xRot != 0.0)
    T3::currentPiece->setAnimXRotation(xRot < 0 ? xRot + 5.0 : xRot - 5.0);

  float zRot = T3::currentPiece->getAnimZRotation();
  if (zRot != 0.0)
    T3::currentPiece->setAnimZRotation(zRot < 0 ? zRot + 5.0 : zRot - 5.0);
  
								 
  glutPostRedisplay();
  T3::frameCount++;

}

/*****************************************************************************
 * Display Functions
 ****************************************************************************/

void T3::drawScreen() {

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  
  glTranslated(0, 0, -2);
  /* everything is drawn with Z coordinate (0,0,-1, 0,0,1), but the view
     frustum has Z coordinates (0,0,1 to 0,0,3). Moving everything forward 2
     units corrects this. */

  T3::drawStarfield();
  T3::drawPlayingColumn();

  glutSwapBuffers();
  glFlush();


}

void T3::drawStarfield() {

	struct Star* stars = T3::starfield->getStars();
	
	for (int i = 0; i < 50; i++) {
		
		glPushMatrix();
		glTranslated(stars[i].x, stars[i].y, -1.0);
		glScaled(0.3 * stars[i].p, 0.3 * stars[i].p, 0.3 * stars[i].p);
		glTranslated(0.45, 0.95, -0.45);
		T3::drawPiece(T3::backgroundPieces[i], true, false);
		glPopMatrix();			
		
		T3::backgroundPieces[i]->setAnimXRotation(
			T3::backgroundPieces[i]->getAnimXRotation() + (3 * stars[i].p));
			
		T3::backgroundPieces[i]->setAnimYRotation(
			T3::backgroundPieces[i]->getAnimYRotation() + (stars[i].p));
			
		T3::backgroundPieces[i]->setAnimZRotation(
			T3::backgroundPieces[i]->getAnimZRotation() + (stars[i].p));
		
	}
	
	glPointSize(2);
	glNormal3f(0.0, 0.0, 1.0);
	glBegin(GL_POINTS);
	
	for (int i = 0; i < T3::starfield->getStarCount(); i++) {
	
		glColor3f(stars[i].p + T3::starMagentaFlash,
		stars[i].p + T3::starGreenFlash / 2,
		stars[i].p + 0.4 + T3::starMagentaFlash);
		
		glVertex3f(stars[i].x, stars[i].y, -1.1);
		
	}
	
	if (T3::starMagentaFlash > 0)
		T3::starMagentaFlash -= 0.005;
	
	if (T3::starGreenFlash > 0)
		T3::starGreenFlash -= 0.005;
	
	glEnd();

}

void T3::drawPlayingColumn() {

  glPushMatrix();

  if (T3::isTimerEnabled) {

    if (T3::playerTimeRemaining < 31) {

      GLfloat light_color[] =
	{1.0 - ((31 - T3::playerTimeRemaining) / 30.0),
	 1.0 - ((31 - T3::playerTimeRemaining) / 30.0),
	 1.0 - ((31 - T3::playerTimeRemaining) / 30.0), 0.0};

      glLightfv(GL_LIGHT0, GL_DIFFUSE, light_color);
      glLightfv(GL_LIGHT0, GL_SPECULAR, light_color);
      glEnable(GL_LIGHT0);

    } else {

      GLfloat light_color[] = {1.0, 1.0, 1.0, 0.0};
      glLightfv(GL_LIGHT0, GL_DIFFUSE, light_color);
      glLightfv(GL_LIGHT0, GL_SPECULAR, light_color);
      glEnable(GL_LIGHT0);

    }

  }

  glScaled(0.5, 0.5, 0.5);
  // ^_ scale playing field down to 1/2 the view frustum

  glRotatef(T3::playingFieldTilt, 1.0, 0.0, 0.0);
  glRotatef(T3::playingFieldRotation, 0.0, 1.0, 0.0);
  // ^_ perform view angle rotations on the playing field
  
  double offset = ((10 - T3::gameSize) / 2) / 10.0;
  glTranslated(offset, 0, -offset);
  // ^_ center the playing field on the screen

  T3::drawPlayingColumnFloor();
  T3::drawPlayingColumnPosts();
  T3::drawLevelIndicators();

  if (!T3::isPaused || T3::isGameOver) {

    T3::drawLandedBlocks();
    T3::drawDropShadow();

  }

  T3::drawPiece(T3::currentPiece, true, true);
  T3::drawGhostBlocks();

  glPopMatrix();

  glPushMatrix();    
  
  glRotated(T3::nextPieceRotation, 1.0, 0.75, 0.5);
  glTranslated(-.5, 1, -.5);
  glScaled(0.5, 0.5, 0.5);
  T3::drawPiece(T3::nextPiece, true, false);

  T3::nextPieceRotation++;
  if (T3::nextPieceRotation == 360)
  	T3::nextPieceRotation = 0;
  	
  glPopMatrix();
  
  T3::drawScoreboards();

  if (T3::isPaused)
    if (T3::isGameOver) {
      T3::drawText(-0.15, 0.1, 0.9, 4, false, "Game");
      T3::drawText(-0.15, 0.0, 0.9, 4, false, "Over");
    } else
      T3::drawText(-0.25, 0.05, 0.9, 4, false, "Paused");

}

void T3::drawPlayingColumnFloor() {

	glPushMatrix();

	for (int i = 0; i < T3::gameSize; i++)
		for (int j = 0; j < T3::gameSize; j++)
			if ((i == 0 || i == T3::gameSize - 1 || j == 0 || j == T3::gameSize - 1) || T3::isPaused ||
			T3::landedBlocks[0][i][j] == NULL ||
			T3::surfaceYTranslation[0] > 0) {
								
				class RGB* rgb = T3::floorBlocks[i][j]->getRGB();
				glColor3f(rgb->getR(), rgb->getG(), rgb->getB());
				T3::drawBlock(T3::floorBlocks[i][j], true, true);
					
			}

  glPopMatrix();

}

void T3::drawPlayingColumnPosts() {

  glPushMatrix();

  for (int i = 0; i < 21; i++)
    for (int j = 0; j < 4; j++) {

      if (i - 1 >= T3::currentPiece->getMinXYZ()->getY() &&
		 i - 1 <= T3::currentPiece->getMaxXYZ()->getY()) {
	
	glColor3f(0.0, 0.3, 0.0);

      } else if (i > 0 && i < 20 && !T3::isSurfaceAlive[i - 1]) {
	
	glColor3f(0.3, 0.3, 0.3);

      } else {

	class RGB* rgb = T3::postBlocks[i][j]->getRGB();
	glColor3f(rgb->getR(), rgb->getG(), rgb->getB());

      }

      T3::drawBlock(T3::postBlocks[i][j], true, true);

    }

  glPopMatrix();  

}

void T3::drawScoreboards() {
	
	char str[12];
	
	if (T3::showFPSOnScoreboard)
	
		sprintf(str, "FPS: %-3i    ", (int)(T3::frameCount /
		((float)(clock() - T3::firstClockTick) / CLOCKS_PER_SEC)));
	
	else if (T3::showTimeOnScoreboard)
	
		if (T3::isTimerEnabled)
	
			sprintf(str, "%2i:%02i  %2i:%02i",
			T3::playerTimeAccumulated / 60, T3::playerTimeAccumulated % 60,
			T3::playerTimeTicker / 60, T3::playerTimeTicker % 60);
	
		else
	
			sprintf(str, "%2i:%02i       ",
			T3::playerTimeAccumulated / 60, T3::playerTimeAccumulated % 60);
	
	else
	
		sprintf(str, "%-3i %08li", T3::playerSurfacesCompleted, T3::playerScoreTicker);

	glPushMatrix();
	glScaled(0.5, 0.5, 0.5);
	T3::drawText(-0.55, -0.9, 1, 4, false, str);
	glPopMatrix();

	if (T3::playerScoreTicker < T3::playerScore)	
		T3::playerScoreTicker += (long)ceil((T3::playerScore - T3::playerScoreTicker) / 10.0);
		
	if (T3::playerTimeTicker < T3::playerTimeRemaining)
		T3::playerTimeTicker += (int)ceil((T3::playerTimeRemaining - T3::playerTimeTicker) / 10.0);
	else
		T3::playerTimeTicker = T3::playerTimeRemaining;

}

void T3::drawLevelIndicators() {

  char str[3];
  sprintf(str, "L%-1i", T3::playerLevel);

  for (int j = 0; j < 4; j++) {

    class wXYZ xyz = T3::postBlocks[20][j]->getXYZ()->asWorldXYZ();
    float cx = xyz.getX(); float cy = xyz.getY(); float cz = xyz.getZ();
    
    if (T3::visibleBlockSurfaces[4])
      T3::drawText(cx, cy, cz, 4, true, str);
	
    if (T3::visibleBlockSurfaces[5])
      T3::drawText(cx, cy, cz, 5, true, str);
      
    if (T3::visibleBlockSurfaces[2])
      T3::drawText(cx, cy, cz, 2, true, str);
      
    if (T3::visibleBlockSurfaces[3])
      T3::drawText(cx, cy, cz, 3, true, str);
      
  }

}

void T3::drawLandedBlocks() {


  list<class Block*>::const_iterator i;
  for (i = T3::visibleBlocks.begin(); i != T3::visibleBlocks.end(); ++i) {

    glPushMatrix();
    
    if (T3::surfaceYTranslation[(*i)->getXYZ()->getY()] > 0)
      glTranslated(0.0, T3::surfaceYTranslation[(*i)->getXYZ()->getY()], 0.0);

    class RGB* rgb = (*i)->getRGB();
    
    int c = ((*i)->getXYZ()->getY() + T3::surfaceColorOffset) % 7;
    
    if (rgb->getR() < T3::colorScheme[c][0])
      (*i)->setRGB(rgb->getR() + 0.01, rgb->getG(), rgb->getB());
    
    else if (rgb->getR() > T3::colorScheme[c][0])
      (*i)->setRGB(rgb->getR() - 0.01, rgb->getG(), rgb->getB());

    if (rgb->getG() < T3::colorScheme[c][1])
      (*i)->setRGB(rgb->getR(), rgb->getG() + 0.01, rgb->getB());
    
    else if (rgb->getG() > T3::colorScheme[c][1])
      (*i)->setRGB(rgb->getR(), rgb->getG() - 0.01, rgb->getB());

    if (rgb->getB() < T3::colorScheme[c][2])
      (*i)->setRGB(rgb->getR(), rgb->getG(), rgb->getB() + 0.01);
    
    else if (rgb->getB() > T3::colorScheme[c][2])
      (*i)->setRGB(rgb->getR(), rgb->getG(), rgb->getB() - 0.01);

    glColor3f(rgb->getR(), rgb->getG(), rgb->getB());
  
    T3::drawBlock(*i, true, true);

    glPopMatrix();

  }

  for (int i = 0; i < 20; i++)
    if (T3::surfaceYTranslation[i] > 0 && T3::ghostBlockAlpha <= 0)
      T3::surfaceYTranslation[i] -= 0.01;

}

void T3::drawGhostBlocks() {

  if (T3::ghostBlockAlpha > 0.0) {

    glPushMatrix();

    for (list<class Block*>::const_iterator i = T3::ghostBlocks.begin();
	 i != T3::ghostBlocks.end(); ++i) {
    
      class RGB* rgb = (*i)->getRGB();
      glColor4f(rgb->getR(), rgb->getG(), rgb->getB(), T3::ghostBlockAlpha);

      T3::drawBlock(*i, true, false);

    }

    glPopMatrix();

    T3::ghostBlockAlpha -= 0.01;

  }

}

void T3::drawPiece(class Piece* p, bool doTextures, bool doTransparency) {
  
  class wXYZ xyz = p->getBlock(0)->getXYZ()->asWorldXYZ();
  float cx = xyz.getX(); float cy = xyz.getY(); float cz = xyz.getZ();

  glPushMatrix();

  glTranslated(cx, cy, cz);

  float yTrans = p->getAnimYTranslation();
  if (yTrans > 0)
    glTranslated(0.0, yTrans, 0.0);

  float xRot = p->getAnimXRotation();
  if (xRot != 0.0)
    glRotated(xRot, 1.0, 0.0, 0.0);

  float yRot = p->getAnimYRotation();
  if (yRot != 0.0)
    glRotated(yRot, 0.0, 1.0, 0.0);

  float zRot = p->getAnimZRotation();
  if (zRot != 0.0)
    glRotated(zRot, 0.0, 0.0, 1.0);

  glTranslated(-cx, -cy, -cz);

  for (int i = 0; i < 4; i++) {

    class RGB* rgb = p->getBlock(i)->getRGB();
    if (doTransparency)
      glColor4f(rgb->getR(), rgb->getG(), rgb->getB(), 0.6);
    else
      glColor4f(rgb->getR(), rgb->getG(), rgb->getB(), 1.0);

    drawBlock(p->getBlock(i), doTextures, false);

  }

  glPopMatrix();

}

void T3::drawDropShadow() {

  int dy = 0;
  while (T3::requestPieceTranslation(0, -1, 0)) {dy++;}

  glPushMatrix();
  glColor4f(0.1, 0.1, 0.1, 0.8);

  for (int i = 0; i < 4; i++) {

    class Block* b = T3::currentPiece->getBlock(i);
    class bXYZ* ixyz = b->getXYZ();
    
    if (ixyz->getY() == 0 ||
	T3::landedBlocks[ixyz->getY() - 1]
	[ixyz->getX()][ixyz->getZ()] != NULL) {

      class wXYZ fxyz = ixyz->asWorldXYZ();

      glBegin(GL_POLYGON);
      glVertex3f(fxyz.getX() - 0.05, fxyz.getY() - 0.049, fxyz.getZ() - 0.05);
      glVertex3f(fxyz.getX() - 0.05, fxyz.getY() - 0.049, fxyz.getZ() + 0.05);
      glVertex3f(fxyz.getX() + 0.05, fxyz.getY() - 0.049, fxyz.getZ() + 0.05);
      glVertex3f(fxyz.getX() + 0.05, fxyz.getY() - 0.049, fxyz.getZ() - 0.05);
      glEnd();

    }

  }

  T3::currentPiece->translate(0, dy, 0);

  glPopMatrix();

}

void T3::drawBlock(const class Block* blck, bool doTextures, bool doCulling) {

  class wXYZ xyz = blck->getXYZ()->asWorldXYZ();
  float cx = xyz.getX(); float cy = xyz.getY(); float cz = xyz.getZ();

  //int mode = (doTextures ? GL_QUADS : GL_LINES);
    
  glPushMatrix();
  
  if (doTextures) {
    glEnable(GL_TEXTURE_2D);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_BLEND);
    glBindTexture(GL_TEXTURE_2D, T3::blockTexture);
  }

  
  if (!doCulling || T3::visibleBlockSurfaces[0]) {

    glBegin(doTextures ? GL_POLYGON : GL_LINE_LOOP);

    glNormal3f(0.0, 1.0, 0.0);
    glTexCoord2f(0.0, 0.0); glVertex3f(cx - 0.05, cy + 0.05, cz - 0.05);
    glTexCoord2f(0.0, 1.0); glVertex3f(cx - 0.05, cy + 0.05, cz + 0.05);
    glTexCoord2f(1.0, 1.0); glVertex3f(cx + 0.05, cy + 0.05, cz + 0.05);
    glTexCoord2f(1.0, 0.0); glVertex3f(cx + 0.05, cy + 0.05, cz - 0.05);
    
    glEnd();

  }
  
  if (!doCulling || T3::visibleBlockSurfaces[1]) {

    glBegin(doTextures ? GL_POLYGON : GL_LINE_LOOP);

    glNormal3f(0.0, -1.0, 0.0);
    glTexCoord2f(0.0, 0.0); glVertex3f(cx - 0.05, cy - 0.05, cz - 0.05);
    glTexCoord2f(0.0, 1.0); glVertex3f(cx - 0.05, cy - 0.05, cz + 0.05);
    glTexCoord2f(1.0, 1.0); glVertex3f(cx + 0.05, cy - 0.05, cz + 0.05);
    glTexCoord2f(1.0, 0.0); glVertex3f(cx + 0.05, cy - 0.05, cz - 0.05);

    glEnd();

  }

  if (!doCulling || T3::visibleBlockSurfaces[2]) {
    
    glBegin(doTextures ? GL_POLYGON : GL_LINE_LOOP);

    glNormal3f(-1.0, 0.0, 0.0);
    glTexCoord2f(0.0, 0.0); glVertex3f(cx - 0.05, cy - 0.05, cz - 0.05);
    glTexCoord2f(0.0, 1.0); glVertex3f(cx - 0.05, cy - 0.05, cz + 0.05);
    glTexCoord2f(1.0, 1.0); glVertex3f(cx - 0.05, cy + 0.05, cz + 0.05);
    glTexCoord2f(1.0, 0.0); glVertex3f(cx - 0.05, cy + 0.05, cz - 0.05);

    glEnd();

  }

  if (!doCulling || T3::visibleBlockSurfaces[3]) {

    glBegin(doTextures ? GL_POLYGON : GL_LINE_LOOP);

    glNormal3f(1.0, 0.0, 0.0);
    glTexCoord2f(0.0, 0.0); glVertex3f(cx + 0.05, cy - 0.05, cz - 0.05);
    glTexCoord2f(0.0, 1.0); glVertex3f(cx + 0.05, cy - 0.05, cz + 0.05);
    glTexCoord2f(1.0, 1.0); glVertex3f(cx + 0.05, cy + 0.05, cz + 0.05);
    glTexCoord2f(1.0, 0.0); glVertex3f(cx + 0.05, cy + 0.05, cz - 0.05);
    
    glEnd();

  }

  if (!doCulling || T3::visibleBlockSurfaces[4]) {

    glBegin(doTextures ? GL_POLYGON : GL_LINE_LOOP);

    glNormal3f(0.0, 0.0, 1.0);
    glTexCoord2f(0.0, 0.0); glVertex3f(cx - 0.05, cy - 0.05, cz + 0.05);
    glTexCoord2f(0.0, 1.0); glVertex3f(cx + 0.05, cy - 0.05, cz + 0.05);
    glTexCoord2f(1.0, 1.0); glVertex3f(cx + 0.05, cy + 0.05, cz + 0.05);
    glTexCoord2f(1.0, 0.0); glVertex3f(cx - 0.05, cy + 0.05, cz + 0.05);

    glEnd();

  }

  if (!doCulling || T3::visibleBlockSurfaces[5]) {

    glBegin(doTextures ? GL_POLYGON : GL_LINE_LOOP);

    glNormal3f(0.0, 0.0, -1.0);
    glTexCoord2f(0.0, 0.0); glVertex3f(cx - 0.05, cy - 0.05, cz - 0.05);
    glTexCoord2f(0.0, 1.0); glVertex3f(cx + 0.05, cy - 0.05, cz - 0.05);
    glTexCoord2f(1.0, 1.0); glVertex3f(cx + 0.05, cy + 0.05, cz - 0.05);
    glTexCoord2f(1.0, 0.0); glVertex3f(cx - 0.05, cy + 0.05, cz - 0.05);

    glEnd();

  }

  if (doTextures)
    glDisable(GL_TEXTURE_2D);

  glPopMatrix();

}

void T3::drawText(float cx, float cy, float cz, int face, bool vertical, 
		  char* str) {

  glPushMatrix();
  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

  int i = 0;
  while (str[i] != 0) {

    if (str[i] >= '0' && str[i] <= '9')
      glBindTexture(GL_TEXTURE_2D, T3::charTextures[str[i] - 21]);
    
    else if (str[i] >= 'a' && str[i] <= 'z')
      glBindTexture(GL_TEXTURE_2D, T3::charTextures[str[i] - 96]);

    else if (str[i] >= 'A' && str[i] <= 'Z')
      glBindTexture(GL_TEXTURE_2D, T3::charTextures[str[i] - 64]);

    else if (str[i] == ':')
      glBindTexture(GL_TEXTURE_2D, T3::charTextures[37]);

    else
      glBindTexture(GL_TEXTURE_2D, T3::charTextures[0]);

    switch (face) {

    case 2:

      glBegin(GL_QUADS);
      glNormal3f(-1.0, 0.0, 0.0);
      glTexCoord2f(0.0, 0.0); glVertex3f(cx - 0.051, cy - 0.05, cz - 0.05);
      glTexCoord2f(0.0, 1.0); glVertex3f(cx - 0.051, cy + 0.05, cz - 0.05);
      glTexCoord2f(1.0, 1.0); glVertex3f(cx - 0.051, cy + 0.05, cz + 0.05);
      glTexCoord2f(1.0, 0.0); glVertex3f(cx - 0.051, cy - 0.05, cz + 0.05);
      glEnd();

      if (!vertical)
	cz += 0.1;
      
      break;

    case 3:

      glBegin(GL_QUADS);
      glNormal3f(1.0, 0.0, 0.0);
      glTexCoord2f(0.0, 0.0); glVertex3f(cx + 0.051, cy - 0.05, cz - 0.05);
      glTexCoord2f(0.0, 1.0); glVertex3f(cx + 0.051, cy + 0.05, cz - 0.05);
      glTexCoord2f(-1.0, 1.0); glVertex3f(cx + 0.051, cy + 0.05, cz + 0.05);
      glTexCoord2f(-1.0, 0.0); glVertex3f(cx + 0.051, cy - 0.05, cz + 0.05);
      glEnd();

      if (!vertical)
	cz -= 0.1;

      break;

    case 4:

      glBegin(GL_QUADS);
      glNormal3f(0.0, 0.0, 1.0);
      glTexCoord2f(0.0, 0.0); glVertex3f(cx - 0.05, cy - 0.05, cz + 0.051);
      glTexCoord2f(1.0, 0.0); glVertex3f(cx + 0.05, cy - 0.05, cz + 0.051);
      glTexCoord2f(1.0, 1.0); glVertex3f(cx + 0.05, cy + 0.05, cz + 0.051);
      glTexCoord2f(0.0, 1.0); glVertex3f(cx - 0.05, cy + 0.05, cz + 0.051);
      glEnd();

      if (!vertical)
	cx += 0.1;

      break;

    case 5:

      glBegin(GL_QUADS);
      glNormal3f(0.0, 0.0, -1.0);
      glTexCoord2f(1.0, -1.0); glVertex3f(cx - 0.05, cy - 0.05, cz - 0.051);
      glTexCoord2f(0.0, -1.0); glVertex3f(cx + 0.05, cy - 0.05, cz - 0.051);
      glTexCoord2f(0.0, 0.0); glVertex3f(cx + 0.05, cy + 0.05, cz - 0.051);
      glTexCoord2f(1.0, 0.0); glVertex3f(cx - 0.05, cy + 0.05, cz - 0.051);
      glEnd();

      if (!vertical)
	cx -= 0.1;

      break;
      
    }

    if (vertical)
      cy -= 0.1;

    i++;

  }

  glDisable(GL_TEXTURE_2D);
  glPopMatrix();

}

void T3::handleWindowResizeEvent(int width, int height) {

  T3::oldWindowWidth = T3::windowWidth;
  T3::oldWindowHeight = T3::windowHeight;

  T3::windowWidth = width;
  T3::windowHeight = height;

  // Update the viewport
  glViewport(0, 0, T3::windowWidth, T3::windowHeight);

  // Update the viewing frustum
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, T3::windowWidth/(float)T3::windowHeight, 1.0, 4.0);

}

/*****************************************************************************
 * Occlusion & Culling Functions
 ****************************************************************************/

void T3::updateVisibleBlocks() {

	T3::visibleBlocks.clear();
	
	for (int i = 0; i < 20; i++) {
		
		for (int j = 0; j < 10; j++) {
			
			for (int k = 0; k < 10; k++) {
				
				if (T3::landedBlocks[i][j][k] != NULL) {
	
					if ((T3::visibleBlockSurfaces[0] &&
					(i == 19 || T3::landedBlocks[i + 1][j][k] == NULL ||
					 T3::surfaceYTranslation[i + 1] > 0)) ||
					(T3::visibleBlockSurfaces[2] &&
					 (j == 0 || T3::landedBlocks[i][j - 1][k] == NULL)) ||
					(T3::visibleBlockSurfaces[3] &&
					 (j == 9 || T3::landedBlocks[i][j + 1][k] == NULL)) ||
					(T3::visibleBlockSurfaces[4] &&
					 (k == 0 || T3::landedBlocks[i][j][k - 1] == NULL)) ||
					(T3::visibleBlockSurfaces[5] &&
					 (k == 9 || T3::landedBlocks[i][j][k + 1] == NULL)))
						
						T3::visibleBlocks.push_back(T3::landedBlocks[i][j][k]);
	
				}
				
			}
			
		}
		
	}

}

void T3::updateVisibleBlockSurfaces() {

  // Top
  T3::visibleBlockSurfaces[0] =
    true;
  
  // Bottom
  T3::visibleBlockSurfaces[1] =
    (T3::playingFieldTilt > 0);

  // Left
  T3::visibleBlockSurfaces[2] = 
    ((T3::playingFieldRotation >= 0 &&
      T3::playingFieldRotation <= 180 + T3::playingFieldTilt) ||
     T3::playingFieldRotation >= 360 - T3::playingFieldTilt);

  // Right
  T3::visibleBlockSurfaces[3] =
    ((T3::playingFieldRotation >= 180 - T3::playingFieldTilt &&
      T3::playingFieldRotation <= 360) ||
     T3::playingFieldRotation <= 0 + T3::playingFieldTilt);

  // Front
  T3::visibleBlockSurfaces[4] =
    ((T3::playingFieldRotation >= 0 &&
      T3::playingFieldRotation <= 90 + T3::playingFieldTilt) ||
     (T3::playingFieldRotation >= 270 - T3::playingFieldTilt &&
      T3::playingFieldRotation <= 360));

  // Back
  T3::visibleBlockSurfaces[5] =
    ((T3::playingFieldRotation >= 90 - T3::playingFieldTilt &&
      T3::playingFieldRotation <= 270 + T3::playingFieldTilt));


}

/*****************************************************************************
 * Player Control Functions
 ****************************************************************************/

void T3::setPaused(bool paused) {

  if (!T3::isGameOver) {

    T3::isPaused = paused;

    if (T3::isPaused) {

      if (!T3::isFullScreen)
	glutSetCursor(GLUT_CURSOR_INHERIT);

    } else {

      glutSetCursor(GLUT_CURSOR_NONE);
      T3::playerTimer = clock();
      T3::pieceTimer = clock();

    }

  }

}

void T3::handleKeyboardEvent(int key, int x, int y) {

  switch (key) {

  case GLUT_KEY_F1:

    T3::reset();
    break;

  case GLUT_KEY_F12:
    
    T3::showFPSOnScoreboard = !T3::showFPSOnScoreboard;
    break;

  }

}

void T3::handleKeyboardEvent(unsigned char key, int x, int y) {

  if (T3::isPaused && key != 27 && key != 'f' && key != 'p' && key != 9)
    T3::setPaused(false);

  int i = (int)T3::playingFieldRotation / 45;
  
  if (i == 8)
    i = 7;
  
  switch (key) {

  case 'w':
  case 'W':

    if (!T3::isPaused)
      T3::requestPieceTranslation
	(T3::wasdRotationMatrix[i][4], 0, T3::wasdRotationMatrix[i][5]);

    break;

  case 's':
  case 'S':
    
    if (!T3::isPaused)
      T3::requestPieceTranslation
	(T3::wasdRotationMatrix[i][0], 0, T3::wasdRotationMatrix[i][1]);

    break;

  case 'a':
  case 'A':

    if (!T3::isPaused)
      T3::requestPieceTranslation
	(T3::wasdRotationMatrix[i][6], 0, T3::wasdRotationMatrix[i][7]);

    break;

  case 'd':
  case 'D':

    if (!T3::isPaused)
      T3::requestPieceTranslation
	(T3::wasdRotationMatrix[i][2], 0, T3::wasdRotationMatrix[i][3]);

    break;

  case 'q':
  case 'Q':

    if (!T3::isPaused) {
      if (T3::requestPieceRotation
	  (T3::qerzRotationMatrix[i][10],
	   T3::qerzRotationMatrix[i][11],
	   T3::qerzRotationMatrix[i][12])) {

	T3::currentPiece->setAnimXRotation(T3::qerzRotationMatrix[i][13]);
	T3::currentPiece->setAnimZRotation(T3::qerzRotationMatrix[i][14]);
	
      }
    }

    break;

  case 'z':
  case 'Z':

    if (!T3::isPaused) {
      if (T3::requestPieceRotation
	  (T3::qerzRotationMatrix[i][0],
	   T3::qerzRotationMatrix[i][1],
	   T3::qerzRotationMatrix[i][2])) {
	
	T3::currentPiece->setAnimXRotation(T3::qerzRotationMatrix[i][3]);
	T3::currentPiece->setAnimZRotation(T3::qerzRotationMatrix[i][4]);

      }
    }
    break;

  case 'e':
  case 'E':
    
    if (!T3::isPaused) {
      if (T3::requestPieceRotation
	  (T3::qerzRotationMatrix[i][15],
	   T3::qerzRotationMatrix[i][16],
	   T3::qerzRotationMatrix[i][17])) {

	T3::currentPiece->setAnimXRotation(T3::qerzRotationMatrix[i][18]);
	T3::currentPiece->setAnimZRotation(T3::qerzRotationMatrix[i][19]);

      }
    }
    
    break;

  case 'r':
  case 'R':
    
    if (!T3::isPaused) {
      if (T3::requestPieceRotation
	  (T3::qerzRotationMatrix[i][5],
	   T3::qerzRotationMatrix[i][6],
	   T3::qerzRotationMatrix[i][7])) {
	  
	T3::currentPiece->setAnimXRotation(T3::qerzRotationMatrix[i][8]);
	T3::currentPiece->setAnimZRotation(T3::qerzRotationMatrix[i][9]);

      }
    }

    break;

  case 'f':
  case 'F':

    if (!T3::isFullScreen) {

      glutFullScreen();
      T3::isFullScreen = true;

      if (T3::isPaused)
	glutSetCursor(GLUT_CURSOR_NONE);

    } else {

      glutReshapeWindow(oldWindowWidth, oldWindowHeight);
      glutPositionWindow(100, 100);
      T3::isFullScreen = false;

      if (T3::isPaused)
	glutSetCursor(GLUT_CURSOR_INHERIT);

    }

    break;    

  case 'p':
  case 'P':

    T3::setPaused(!T3::isPaused);
    break;

  case 32:

    if (!T3::isPaused) {

      while (T3::requestPieceTranslation(0, -1, 0)) { }
      T3::handlePieceLanding();

    }

    break;

  case 27:

	glutDestroyWindow(T3::window);
    T3::cleanup();
    exit(0);
    break;

  }

}


void T3::handleMouseMotionEvent(int x, int y) {

  int dx = x - T3::mouseLastX;
  int dy = y - T3::mouseLastY;
  
  if (!T3::isPaused || T3::isFullScreen) {

    if (x < 0 || x >= T3::windowWidth - 2) {

      x = T3::windowWidth / 2;
      T3::mouseLastX = x - dx;
      glutWarpPointer(x, y);

    } 
    
    if (y < 0 || y >= T3::windowHeight - 2) {

      y = T3::windowHeight / 2;;
      T3::mouseLastY = y - dy;
      glutWarpPointer(x, y);

    } 

  }

  switch (T3::mouseActiveButton) {

  case GLUT_LEFT_BUTTON:

    if (!T3::isGameOver && x != T3::mouseMarkedX) {

      float yRot = T3::currentPiece->getAnimYRotation();

      if (T3::isPaused)
		T3::setPaused(false);

      yRot -= dx / 2.0;
    
      if (yRot > 45.0) {

	if (T3::requestPieceRotation(1, 0, -1))
	  yRot = 0.0;
	else
	  yRot = 45.0;
      
      } else if (yRot < -45.0) {

	if (T3::requestPieceRotation(-1, 0, 1))
	  yRot = 0.0;
	else
	  yRot = -45.0;

      }

      T3::mouseMarkedX = x;
      T3::mouseMarkedY = y;

      T3::currentPiece->setAnimYRotation(yRot);

    }

    break;

  default:

    T3::playingFieldTilt += dy / 2.0;

    if (T3::playingFieldTilt > 90)
      T3::playingFieldTilt = 90;

    else if (T3::playingFieldTilt < 0)
      T3::playingFieldTilt = 0;
    
    T3::playingFieldRotation += dx / 2.0;
  
    if (T3::playingFieldRotation > 360)
      T3::playingFieldRotation = 0;

    else if (T3::playingFieldRotation < 0)
      T3::playingFieldRotation = 360;    
    
    T3::updateVisibleBlockSurfaces();
    T3::updateVisibleBlocks();


  }

  T3::mouseLastX = x;
  T3::mouseLastY = y;

}

void T3::handleMouseEvent(int button, int state, int x, int y) {

  if (state == GLUT_DOWN) {

    T3::mouseActiveButton = button;
    T3::mouseMarkedX = x;
    T3::mouseMarkedY = y;

    if (button == GLUT_RIGHT_BUTTON)
      T3::showTimeOnScoreboard = !T3::isTimerEnabled;
    
  } else if (state == GLUT_UP) {

    T3::mouseActiveButton = -1;

    if (button == GLUT_LEFT_BUTTON) {

      float yRot = T3::currentPiece->getAnimYRotation();
	
      int k = 0;
      if ((yRot >= 0 && yRot < 45) ||
		  yRot > 315)
		k = 0;
	
	  if (yRot > 45 && yRot <= 135)
		k = 1;
	
	  if (yRot > 135 && yRot <= 225)
		k = 2;
	
	  if (yRot > 225 && yRot <= 315)
		k = 3;
	
	  for (int i = 0; i < k; i++)
		T3::requestPieceRotation(1, 0, -1);
	      
			T3::currentPiece->setAnimYRotation(0.0);
	
	  } else if (button == GLUT_RIGHT_BUTTON)
		T3::showTimeOnScoreboard = T3::isTimerEnabled;

  }

  T3::mouseLastX = x;
  T3::mouseLastY = y;

}

/*****************************************************************************
 * Piece Related Functions
 ****************************************************************************/

bool T3::spawnPiece() {

	if (T3::currentPiece != NULL)
		delete T3::currentPiece;
	
	T3::currentPiece = T3::nextPiece;
	T3::nextPiece = T3::generateNewPiece();
	  
	T3::currentPiece->translate
		(T3::gameSize / 2, 19 - T3::currentPiece->getMaxXYZ()->getY() -
		T3::currentPiece->getBlock(0)->getXYZ()->getY(), T3::gameSize / 2);
	  
	if (T3::collisionWithLandedBlocks()) {
	    
		while (T3::collisionWithLandedBlocks())
		T3::currentPiece->translate(0, 1, 0);
		    
		return false;
	
	}
	  
	T3::pieceTimer = clock();
	T3::pieceFallIterations = 0;
	
	return true;

}

class Piece* T3::generateNewPiece() {
  
  class Piece* p = NULL;
  
  switch ((int)floor((rand() / (RAND_MAX + 1.0)) * 7)) {
    
  case 0:
    p = new class IPiece();
    p->setRGB
      (T3::colorScheme[0][0], T3::colorScheme[0][2], T3::colorScheme[0][3]);
    break;
    
  case 1:
    p = new class JPiece();
    p->setRGB
      (T3::colorScheme[1][0], T3::colorScheme[1][1], T3::colorScheme[1][2]);
    break;

  case 2:
    p = new class OPiece();
    p->setRGB
      (T3::colorScheme[2][0], T3::colorScheme[2][1], T3::colorScheme[2][2]);

    break;

  case 3:
    p = new class ZPiece();
    p->setRGB
      (T3::colorScheme[3][0], T3::colorScheme[3][1], T3::colorScheme[3][2]);

    break;

  case 4:
    p = new class TPiece();
    p->setRGB
      (T3::colorScheme[4][0], T3::colorScheme[4][1], T3::colorScheme[4][2]);

    break;

  case 5:
    p = new class Squidget();
    p->setRGB
      (T3::colorScheme[5][0], T3::colorScheme[5][1], T3::colorScheme[5][2]);

    break;

  case 6:
    p = new class Widget();
    p->setRGB
      (T3::colorScheme[6][0], T3::colorScheme[6][1], T3::colorScheme[6][2]);

    break;

  }

  return p;

}

bool T3::requestPieceTranslation(int dx, int dy, int dz) {

  T3::currentPiece->translate(dx, dy, dz);
  
  if (T3::collisionWithBoundaries() || T3::collisionWithLandedBlocks()) {
    T3::currentPiece->translate(-dx, -dy, -dz);
    return false;
  }

  return true;
    
}

bool T3::requestPieceRotation(int rx, int ry, int rz) {

  class bXYZ oldMinXYZ(*T3::currentPiece->getMinXYZ());
  class bXYZ oldMaxXYZ(*T3::currentPiece->getMaxXYZ());

  T3::currentPiece->rotate(rx, ry, rz);

  if (T3::collisionWithBoundaries()) {

    int dx = 0; int dz = 0;

    if (T3::currentPiece->getMinXYZ()->getX() < 0)
      dx = 0 - T3::currentPiece->getMinXYZ()->getX();

    else if (T3::currentPiece->getMaxXYZ()->getX() > T3::gameSize - 1)
      dx = (T3::gameSize - 1) - T3::currentPiece->getMaxXYZ()->getX();

    if (T3::currentPiece->getMinXYZ()->getZ() < 0)
      dz = 0 - T3::currentPiece->getMinXYZ()->getZ();

    else if (T3::currentPiece->getMaxXYZ()->getZ() > T3::gameSize - 1)
      dz = (T3::gameSize - 1) - T3::currentPiece->getMaxXYZ()->getZ();

    if (!T3::requestPieceTranslation(dx, 0, dz)) {
      T3::currentPiece->rotate(-rx, -ry, -rz);
      return false;
    }

  }

  if (T3::collisionWithLandedBlocks()) {

    int dx = 0; int dz = 0;

    for (int i = 0; i < 4; i++) {

      class Block* b = T3::collisionWithLandedBlocks();

      T3::currentPiece->rotate(-rx, -ry, -rz);
      T3::currentPiece->translate(-dx, 0, -dz);
    
      if (b->getXYZ()->getX() < T3::currentPiece->getMinXYZ()->getX()) {
	dx++;
      }

      else if (b->getXYZ()->getX() > T3::currentPiece->getMaxXYZ()->getX()) {
	dx--;
      }

      else if (b->getXYZ()->getZ() < T3::currentPiece->getMinXYZ()->getZ()) {
	dz++;
      }

      else if (b->getXYZ()->getZ() > T3::currentPiece->getMaxXYZ()->getZ()) {
	dz--;
      }

      else {
	dx -= rx;
	dz -= rz;	
      }

      if (!T3::requestPieceTranslation(dx, 0, dz))
	return false;
	
      T3::currentPiece->rotate(rx, ry, rz);

      if (T3::collisionWithBoundaries()) {
	T3::currentPiece->rotate(-rx, -ry, -rz);
	T3::currentPiece->translate(-dx, 0, -dz);
	return false;
      }

      if (!T3::collisionWithLandedBlocks())
	return true;

    }

    T3::currentPiece->rotate(-rx, -ry, -rz);
    T3::currentPiece->translate(-dx, 0, -dz);
    return false;

  }

  return true;

}

void T3::handlePieceLanding() {

	/* Award points for landing a piece. */
	T3::playerScore += 24 + (3 * T3::playerLevel) - T3::pieceFallIterations;

	/* Transfer the blocks from the piece to landedBlocks. */
	for (int b = 0; b < 4; b++) {
	
		class Block* t = T3::currentPiece->getBlock(b);
		
		int i = t->getXYZ()->getY();
		int j = t->getXYZ()->getX();
		int k = t->getXYZ()->getZ();
		
		T3::landedBlocks[i][j][k] = new Block();
		T3::landedBlocks[i][j][k]->setXYZ(t->getXYZ());
		T3::landedBlocks[i][j][k]->setRGB(t->getRGB());
	
	}
	
	/* Clear the list of "ghost" blocks - this will cancel any ongoing
	 * ghost block animation, and prevent the list from continously growing
	 * as the game progresses. */
	if (T3::ghostBlocks.size() != 0) {
		
		for (list<class Block*>::const_iterator i = T3::ghostBlocks.begin();
		i != T3::ghostBlocks.end(); ++i)
			delete *i;
			
		T3::ghostBlocks.clear();
		
	}
			
	/* Completed surface handling. */	 
	
	int completedSurfaces = 0;
	
	for (int i = T3::currentPiece->getMinXYZ()->getY();
	i <= T3::currentPiece->getMaxXYZ()->getY(); i++) {		
		
		bool surfaceComplete = true;
		 
		for (int j = 0; j < T3::gameSize; j++)
			for (int k = 0; k < T3::gameSize; k++)
				if (T3::landedBlocks[i][j][k] == NULL)
					surfaceComplete = false;
		 			
		if (surfaceComplete) {
						
			// move completed surface blocks to ghost blocks
			for (int j = 0; j < T3::gameSize; j++) {
				for (int k = 0; k < T3::gameSize; k++) {
					T3::landedBlocks[i][j][k]->translate(0, completedSurfaces, 0);
					T3::ghostBlocks.push_back(T3::landedBlocks[i][j][k]);
					T3::landedBlocks[i][j][k] = NULL;
				}
				
			}
			
			completedSurfaces++;

			//  shift higher surfaces down
			for (int m = i; m < 20; m++) {														
				for (int j = 0; j < T3::gameSize; j++) {							
					for (int k = 0; k < T3::gameSize; k++) {
								
						if (m < 19) {
						// ^_ if the block to be shifted is within the playing
						//    field, shift the block down
				
							T3::landedBlocks[m][j][k] =
								T3::landedBlocks[m + 1][j][k];
								
							T3::landedBlocks[m + 1][j][k] = NULL;
				
							if (T3::landedBlocks[m][j][k] != NULL)
								T3::landedBlocks[m][j][k]->translate
									(0, -1, 0);
						
						} else
					
							T3::landedBlocks[m][j][k] = NULL;
							
					}
					
				}

				T3::surfaceYTranslation[m] += 0.1;
											
			}
			
			i--;
						
		}	
		
	}
		 
	/* handling performed after completed surfaces are removed. */
	if (completedSurfaces > 0) {
		
		T3::surfaceColorOffset += completedSurfaces;
		
		/* Set the animation alpha factor for the ghost blocks. */		
		T3::ghostBlockAlpha = 1.0;
		
		/* Award points for completing surfaces. */
		T3::playerScore += (long)(1000 * pow(2, completedSurfaces - 1));
		
		/* If the timer is running, award time for completing surfaces. */
		if (T3::isTimerEnabled)
			T3::playerTimeRemaining += (int)(T3::gameSize * 3 * pow(2, completedSurfaces - 1));
		
		/* Update the player's completed surface counter. */	
		T3::playerSurfacesCompleted += completedSurfaces;
		
		/* Flash the stars magenta if a "tetris" was completed. */
		if (completedSurfaces == 4)
			T3::starMagentaFlash = 1.0;
		 
		/* New level handling - level 9 is the highest obtainable level. */
		if (T3::playerSurfacesCompleted >= (T3::playerLevel + 1) * 10 &&
			T3::playerLevel < 9) {
				
			T3::playerLevel++;
								     
		    /* Award points for reaching a new level. */
			T3::playerScore += 10000 * T3::playerLevel;
			
			/* Flash the stars green (unless they are already going to be
			 * flashed magenta). */
			if (completedSurfaces != 4)
				T3::starGreenFlash = 1.0;
		
		}
	    
	}
	
	/* Determine the maximum block height and check for dead surfaces. */
	int maxBlockHeight = 0;
	for (int i = 0; i < 20; i++) {
	    
		T3::isSurfaceAlive[i] = true;
		
		for (int j = 0; j < T3::gameSize; j++) {
			
			for (int k = 0; k < T3::gameSize; k++) {
			
				if (T3::landedBlocks[i][j][k] == NULL) {
					
					if (i < 19 && T3::landedBlocks[i + 1][j][k] != NULL)
						T3::isSurfaceAlive[i] = false;
						
				} else {
					
					if (i > maxBlockHeight)
						maxBlockHeight = i;
						
				}
				
			}
			
		}
	
	}
	
	/* Update the background starfield scroll speed. */
	T3::starfield->setScrollSpeed((maxBlockHeight + 1) * 0.0025);

	/* Try to spawn in the next piece; if this fails, the game is over. */
	if (!T3::spawnPiece()) {
	
		T3::isPaused = true;
		T3::isGameOver = true;
	
	}
	
	/* Update the list of visible blocks. */
	T3::updateVisibleBlocks();

}

/*****************************************************************************
 * Collision Detection Functions
 ****************************************************************************/

bool T3::collisionWithBoundaries() {

  return (T3::currentPiece->getMinXYZ()->getX() < 0 ||
	  T3::currentPiece->getMaxXYZ()->getX() > T3::gameSize - 1 ||
	  T3::currentPiece->getMinXYZ()->getY() < 0 ||
	  T3::currentPiece->getMaxXYZ()->getY() > 19 ||
	  T3::currentPiece->getMinXYZ()->getZ() < 0 ||
	  T3::currentPiece->getMaxXYZ()->getZ() > T3::gameSize - 1);

}

class Block* T3::collisionWithLandedBlocks() {

  for (int b = 0; b < 4; b++) {
    class bXYZ* xyz = T3::currentPiece->getBlock(b)->getXYZ();
    if (xyz->getX() >= 0 && xyz->getX() <= T3::gameSize - 1 &&
	xyz->getY() >= 0 && xyz->getY() <= 19 &&
	xyz->getZ() >= 0 && xyz->getZ() <= T3::gameSize - 1)
      if (T3::landedBlocks[xyz->getY()][xyz->getX()][xyz->getZ()] != NULL)
	return T3::landedBlocks[xyz->getY()][xyz->getX()][xyz->getZ()];
  }

  return NULL;

}

/*****************************************************************************
 * Utility Functions
 ****************************************************************************/

void T3::loadTextureFromTIFF(GLuint* name, char* filename) {

  TIFF* imageFile = TIFFOpen(filename, "r");

  uint32 width, length;

  TIFFGetField(imageFile, TIFFTAG_IMAGEWIDTH, &width);
  TIFFGetField(imageFile, TIFFTAG_IMAGELENGTH, &length);
  
  uint32* imageData = (uint32*)_TIFFmalloc(width * length * sizeof(uint32));

  TIFFReadRGBAImage(imageFile, width, length, imageData, 0);

  glGenTextures(1, name);
  glBindTexture(GL_TEXTURE_2D, *name);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, length, 0, GL_RGBA,
	       GL_UNSIGNED_BYTE, imageData);

  TIFFClose(imageFile);  
  _TIFFfree(imageData);
  
}

const float T3::colorScheme[7][3] = {
	{1.0, 0.0, 0.0},		// Red
	{1.0, 0.5, 0.0},		// Orange
	{1.0, 1.0, 0.0},		// Yellow
	{0.0, 1.0, 0.0},		// Green
	{0.0, 0.0, 1.0},		// Blue
	{0.5, 0.0, 1.0},		// Indigo
	{0.93, 0.54, 0.93}}; 	// Violet
	/* can you sing a rainbow too? */

const int T3::wasdRotationMatrix[8][8] = {
	{0, -1, 1, 0, 0, 1, -1, 0},
	{-1, 0, 0, -1, 1, 0, 0, 1},
	{-1, 0, 0, -1, 1, 0, 0, 1},
	{0, 1, -1, 0, 0, -1, 1, 0},
	{0, 1, -1, 0, 0, -1, 1, 0},
	{1, 0, 0, 1, -1, 0, 0, -1},
	{1, 0, 0, 1, -1, 0, 0, -1},
	{0, -1, 1, 0, 0, 1, -1, 0}};

const int T3::qerzRotationMatrix[8][20] = {
	{0, -1, 1, -90, 0, -1, 1, 0, 0, 90, 0, 1, -1, 90, 0, 1, -1, 0, 0, -90},
	{1, -1, 0, 0, -90, 0, -1, 1, -90, 0, -1, 1, 0, 0, 90, 0, 1, -1, 90, 0},
	{1, -1, 0, 0, -90, 0, -1, 1, -90, 0, -1, 1, 0, 0, 90, 0, 1, -1, 90, 0},
	{0, 1, -1, 90, 0, 1, -1, 0, 0, -90, 0, -1, 1, -90, 0, -1, 1, 0, 0, 90},
	{0, 1, -1, 90, 0, 1, -1, 0, 0, -90, 0, -1, 1, -90, 0, -1, 1, 0, 0, 90},
	{-1, 1, 0, 0, 90, 0, 1, -1, 90, 0, 1, -1, 0, 0, -90, 0, -1, 1, -90, 0},
	{-1, 1, 0, 0, 90, 0, 1, -1, 90, 0, 1, -1, 0, 0, -90, 0, -1, 1, -90, 0},
	{0, -1, 1, -90, 0, -1, 1, 0, 0, 90, 0, 1, -1, 90, 0, 1, -1, 0, 0, -90}};

long T3::playerScore = 0;
int T3::playerLevel = 0;
long T3::playerTimer = 0;
int T3::playerTimeRemaining = 0;
int T3::playerTimeAccumulated = 0;
int T3::playerSurfacesCompleted = 0;
long T3::playerScoreTicker = 0;
int T3::playerTimeTicker = 0;
class Piece* T3::currentPiece = NULL;
class Piece* T3::nextPiece = NULL;
float T3::nextPieceRotation = 0;
long T3::pieceTimer = 0;
int T3::pieceFallIterations = 0;
int T3::surfaceColorOffset = 0;
bool T3::isSurfaceAlive[20];
float T3::surfaceYTranslation[20];
float T3::ghostBlockAlpha = 0.0;
list<class Block*> T3::ghostBlocks;
class Block* T3::landedBlocks[20][10][10];
class Block* T3::postBlocks[21][4];
class Block* T3::floorBlocks[10][10];
bool T3::isPaused = false;
bool T3::isGameOver = false;
bool T3::isTimerEnabled = false;
long T3::firstClockTick = 0;
long T3::frameCount = 0;
int T3::gameSize = 8;
int T3::window = -1;
int T3::windowWidth = 600;
int T3::windowHeight = 600;
int T3::oldWindowWidth = 0;
int T3::oldWindowHeight = 0;
bool T3::isFullScreen = true;
list<class Block*> T3::visibleBlocks;
bool T3::visibleBlockSurfaces[6] = {true, true, true, true, true, true};
float T3::starMagentaFlash = 0.0;
float T3::starGreenFlash = 0.0;
class Starfield* T3::starfield = NULL;
float T3::playingFieldRotation = 0.0;
float T3::playingFieldTilt = 0.0;
bool T3::showTimeOnScoreboard = false;
bool T3::showFPSOnScoreboard = false;
GLuint T3::blockTexture;
GLuint T3::charTextures[38];
int T3::mouseLastX = 0;
int T3::mouseLastY = 0;
int T3::mouseMarkedX = 0;
int T3::mouseMarkedY = 0;
int T3::mouseActiveButton = -1;

class Piece* T3::backgroundPieces[50];
