/*
  This file is part of T^3 - A 3-dimensional Tetris(tm) game

  Copyright (C) 2004 Derek Hausauer
  http://t-3.sourceforge.net
  
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License (LICENSE.TXT) for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "rgb.hpp"

/*****************************************************************************
 * Constructors / Deconstructor
 ****************************************************************************/

RGB::RGB(float r, float g, float b) {

  this->r = r;
  this->g = g;
  this->b = b;

}

/*****************************************************************************
 * Public Instance Methods
 ****************************************************************************/

void RGB::setRGB(const class RGB* rgb) {

  this->r = rgb->r;
  this->g = rgb->g;
  this->b = rgb->b;

}

void RGB::setRGB(float r, float g, float b) {

  this->setR(r);
  this->setG(g);
  this->setB(b);

}

void RGB::setR(float r) {

  this->r = r;

}

float RGB::getR() const {

  return this->r;

}

void RGB::setG(float g) {

  this->g = g;

}

float RGB::getG() const {

  return this->g;

}

void RGB::setB(float b) {

  this->b = b;

}

float RGB::getB() const {

  return this->b;

}

class RGB RGB::operator= (const class RGB* rgb) {

  return RGB(rgb->getR(), rgb->getG(), rgb->getG());

}
