#####
# Standard Make Targets
#####

.PHONY : all
all :
	(cd src ; make)
	@cp src/t3 ./

.PHONY : clean
clean : 
	@rm -fv *~
	@rm -fv t3
	(cd src ; make clean)
	
.PHONY : dist
dist :
	(cd .. ; tar -czvf t3.tgz \
		t3/CHANGELOG \
		t3/LICENSE \
		t3/Makefile \
		t3/README \
		t3/data/*.tif \
		t3/src/*.cpp \
		t3/src/*.hpp \
		t3/src/Makefile)




